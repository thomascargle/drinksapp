package com.example.cocktails.view.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import coil.load
import com.example.cocktails.databinding.DrinkDetailsBinding
import com.example.cocktails.model.DrinkRepo
import com.example.cocktails.view.DrinkState
import com.example.cocktails.viewmodel.VMFactory
import com.example.cocktails.viewmodel.DrinkViewModel


class DrinkDetailFragment: Fragment() {
    private val VMFactory: VMFactory = VMFactory(DrinkRepo)
    val args by navArgs<DrinkDetailFragmentArgs>()
    lateinit var binding: DrinkDetailsBinding
    lateinit var drinkState: DrinkState
    private val viewModel by viewModels<DrinkViewModel> { VMFactory }
    val TAG = "Cocktail fragment"

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DrinkDetailsBinding.inflate(inflater, container, false)
        initViews()
        viewModel.getOneDrinkById(args.iD)
        return binding.root
    }

    fun initViews() {
        with(binding) {
            viewModel.drinksState.observe(viewLifecycleOwner) {state : DrinkState->
                Log.e(TAG, "initViews: ${state.oneDrink}" )
                tvCocktail.text = state.oneDrink.strDrink
                imgCocktail.load(state.oneDrink.strDrinkThumb)
                tvCocktailDescription.text = state.oneDrink.strInstructions
            }
        }
    }
}