package com.example.cocktails.view

import com.example.cocktails.model.remote.objects.DrinkListInCategory
import com.example.cocktails.model.remote.objects.DrinkById
import com.example.cocktails.model.remote.objects.Categories

data class DrinkState(
    var isLoading: Boolean? = false,
    val errorMsg:String? = "You really screwed that up didn't you?",

    val categories: List<Categories.Category> = listOf(),
    val drinksInCategory: List<DrinkListInCategory.Drink> = listOf(),
    val oneDrink: DrinkById.Drink = DrinkById.Drink()
)
