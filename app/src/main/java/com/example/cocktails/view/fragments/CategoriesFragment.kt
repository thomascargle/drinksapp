package com.example.cocktails.view.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.cocktails.databinding.DrinkCategoryItemBinding
import com.example.cocktails.databinding.DrinkCategoriesBinding
import com.example.cocktails.model.DrinkRepo
import com.example.cocktails.view.DrinkState
import com.example.cocktails.view.adapters.CategoriesAdapter
import com.example.cocktails.viewmodel.VMFactory
import com.example.cocktails.viewmodel.DrinkViewModel

class CategoriesFragment : Fragment() {
    private val VMFactory: VMFactory = VMFactory(DrinkRepo)
    private val viewModel by viewModels<DrinkViewModel>() { VMFactory }
    lateinit var binding: DrinkCategoriesBinding
    lateinit var drinkState: DrinkState
    val TAG = "Categories fragment"

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DrinkCategoriesBinding.inflate(inflater, container, false)
        viewModel.getCategories()
        initViews()
        return binding.root
    }

    fun initViews() {

        with(binding) {

            viewModel.drinksState.observe(viewLifecycleOwner) { state ->


                val categoriesAdapter = CategoriesAdapter(state.categories){ strCategory ->
                    val action = CategoriesFragmentDirections
                        .actionCategoriesToInCategory(strCategory)
                    findNavController().navigate(action)

                }
                rvCategories.adapter = categoriesAdapter
                rvCategories.layoutManager = LinearLayoutManager(context)


            }
        }
    }
}