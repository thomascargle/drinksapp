package com.example.cocktails.view.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.cocktails.databinding.DrinkListInCategoryItemBinding
import com.example.cocktails.model.remote.objects.DrinkListInCategory

class DrinkListAdapter(
    var drinks: List<DrinkListInCategory.Drink> = listOf(),
    val onDrinkClicked: (String) -> Unit = {}
) : RecyclerView.Adapter<DrinkListAdapter.DrinksViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DrinksViewHolder {
        return DrinksViewHolder(
            DrinkListInCategoryItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: DrinksViewHolder, position: Int) {
        val drink = drinks[position]
        holder.displayDrinks(drink)
        holder.onDrinkClicked = onDrinkClicked
    }

    override fun getItemCount(): Int = drinks.size

    class DrinksViewHolder(var binding: DrinkListInCategoryItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        var onDrinkClicked: ((String) -> Unit)? = null


        fun displayDrinks(drink: DrinkListInCategory.Drink) {
            val TAG = "Drinks Adapter"

            binding.tvCategoryCocktail.text = drink.strDrink
            binding.ivCategoryCocktail.load(drink.strDrinkThumb)

            binding.ivCategoryCocktail.setOnClickListener {

                val clicked = drink.idDrink
                Log.d(TAG, "displayDrinks: $clicked", )
                onDrinkClicked?.invoke(clicked)

            }

            binding.tvCategoryCocktail.setOnClickListener {

                val clicked = drink.idDrink
                Log.d(TAG, "displayDrinks: $clicked", )
                onDrinkClicked?.invoke(clicked)

            }
        }
    }
}