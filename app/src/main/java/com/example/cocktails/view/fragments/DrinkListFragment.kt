package com.example.cocktails.view.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.cocktails.databinding.DrinkListInCategoryBinding
import com.example.cocktails.model.DrinkRepo
import com.example.cocktails.view.DrinkState
import com.example.cocktails.view.adapters.DrinkListAdapter
import com.example.cocktails.viewmodel.VMFactory
import com.example.cocktails.viewmodel.DrinkViewModel

class DrinkListFragment : Fragment() {
    private val VMFactory: VMFactory = VMFactory(DrinkRepo)
    private val viewModel by viewModels<DrinkViewModel>() { VMFactory }
    lateinit var binding: DrinkListInCategoryBinding
    lateinit var drinkState: DrinkState
    val TAG = "InCategory fragment"
    val args by navArgs<DrinkListFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DrinkListInCategoryBinding.inflate(inflater, container, false)
        viewModel.getDrinksInCategory(args.category)
        initViews()
        return binding.root
    }

    fun initViews() {
        with(binding) {
            rvInCategory.layoutManager = LinearLayoutManager(context)
            rvInCategory.adapter = DrinkListAdapter()

            viewModel.drinksState.observe(viewLifecycleOwner) { state ->

                val drinksAdapter = DrinkListAdapter(state.drinksInCategory) { strDrink ->
                    val action = DrinkListFragmentDirections
                        .actionInCategoryToCocktailFragment(strDrink)
                    findNavController().navigate(action)

                }
                rvInCategory.adapter = drinksAdapter


            }
        }
    }
}