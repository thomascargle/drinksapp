package com.example.cocktails.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.cocktails.model.DrinkRepo
import com.example.cocktails.model.remote.objects.DrinkById
import com.example.cocktails.view.DrinkState
import kotlinx.coroutines.launch

class DrinkViewModel(val repo: DrinkRepo) : ViewModel() {

    private val _drinksState: MutableLiveData<DrinkState> = MutableLiveData(DrinkState())
    val drinksState: LiveData<DrinkState> get() = _drinksState

    fun getCategories() {
        val job = viewModelScope.launch {
            _drinksState.value = DrinkState(
                categories = DrinkRepo.getCategories().categories
            )
        }
    }

    fun getDrinksInCategory(category: String) {
        val job = viewModelScope.launch {
            _drinksState.value = DrinkState(
                drinksInCategory = DrinkRepo.getDrinksInCategory(category).drinks
            )
        }
    }

    fun getOneDrinkById(ID: String) {
        val job = viewModelScope.launch {
            _drinksState.value = DrinkState(
                oneDrink = DrinkRepo.getOneDrinkById(ID).drinks.firstOrNull() ?: DrinkById.Drink()
            )
        }
    }
}