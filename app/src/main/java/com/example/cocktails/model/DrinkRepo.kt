package com.example.cocktails.model

import com.example.cocktails.model.remote.DrinkService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

object DrinkRepo {
    private val drinkService = DrinkService.getInstance()

    suspend fun getCategories() = withContext(Dispatchers.IO) {
        return@withContext drinkService.getCategories()
    }

    suspend fun getDrinksInCategory(category: String) = withContext((Dispatchers.IO)) {
        return@withContext drinkService.getDrinksInCategory(category)
    }

    suspend fun getOneDrinkById(ID: String) = withContext(Dispatchers.IO) {
        return@withContext drinkService.getOneDrinkById(ID)
    }

}