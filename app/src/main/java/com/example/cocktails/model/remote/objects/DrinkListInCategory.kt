package com.example.cocktails.model.remote.objects

data class DrinkListInCategory(
    val drinks: List<Drink>
) {


    data class Drink(
        val idDrink: String,
        val strDrink: String,
        val strDrinkThumb: String
    )
}
