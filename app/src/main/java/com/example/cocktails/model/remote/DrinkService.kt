package com.example.cocktails.model.remote

import com.example.cocktails.model.remote.objects.DrinkListInCategory
import com.example.cocktails.model.remote.objects.DrinkById
import com.example.cocktails.model.remote.objects.Categories
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import retrofit2.http.GET
import retrofit2.http.Query

interface DrinkService {

    companion object {
        private const val BASE_URL = "https://www.thecocktaildb.com"
        private const val CATEGORIESENDPOINT = "/api/json/v1/1/list.php"
        private const val DRINKSINCATEGORY = "/api/json/v1/1/filter.php"
        private const val ONEDRINKBYID = "/api/json/v1/1/lookup.php"

        fun getInstance(): DrinkService = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create()
    }

    @GET(CATEGORIESENDPOINT)
    suspend fun getCategories(@Query("c") c: String = "list"): Categories

    @GET(DRINKSINCATEGORY)
    suspend fun getDrinksInCategory(@Query("c") c: String): DrinkListInCategory

    @GET(ONEDRINKBYID)
    suspend fun getOneDrinkById(@Query("i") c: String): DrinkById

}